#include "HillClimbRoad.h"
namespace hillclimb{
    HillClimbRoad::HillClimbRoad(const int winWidth, const int winHeight){}
    const int MAX_PART_COUNT = 10;

    int HillClimbRoad::getPartCount(){
        return static_cast<int>(this->partCoords.size());
    }

    std::vector<Coordinates> HillClimbRoad::getPartCoords(){
        return this->partCoords;
    }

    void HillClimbRoad::addPart(double x, double y){
        Coordinates partCoord = {
            .x = x,
            .y = y
        };
        this->partCoords.push_back(partCoord);
    }
    
    void HillClimbRoad::move(const double x){
        for(auto& coords: this->partCoords){
            coords.x -= x;
            void generatePartsAhead();
            
        }
    }
    
    void generatePartsAhead(){
        for(int i= 0; i < MAX_PART_COUNT; i++){
            void generateRoadParts();
        }
    }
}

/* Includes

namespace

   ROAD_LENGHT_FACTOR = choose some road length factor

   HillClimbRoad constructor

   function getPartCount

   function getPartCoords

   function addPart

   function calculateNewPartX {
       randomize length of the new part using some factor
   }

   function calculateNewPartY {
       randomize y position of the end point of the new part
   }

   function generatePartsAhead {
      generate parts as many as MAX_PART_COUNT - currentPartCount
   }

   deletePartsBehind() {
      delete parts whose x < -DEFAULT_ROAD_LENGTH * ROAD_LENGTH_FACTOR
   }

   move(x) {
       move the x's of parts
       deletePartsBehind
       generatePartsAhead
   }

   reset
       clear partCoords
       add two horizontal parts
       generatePartsAhead
   }
*/