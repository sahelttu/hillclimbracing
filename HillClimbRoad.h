#ifndef __HILLCLIMBROAD_H_INCLUDED__
#define __HILLCLIMBROAD_H_INCLUDED__

#include "cocos2d.h"
#include "HillClimbUtility.h"
namespace hillclimb{
class HillClimbRoad{
public:
    const int MAX_PART_COUNT = 10;
    //const double X_ROAD_START;
    //const double Y_ROAD_START;
    //const int DEFAULT_ROAD_LENGTH;
    
    HillClimbRoad(const int winWidth, const int winHeight);
    void move(const double x);
    std::vector<Coordinates> getPartCoords();
    int getPartCount();
    void reset();
    void addPart(double x, double y);
private:
    std::vector<Coordinates> partCoords;
    
    //int calculateNewPartX(prevPartX);
    double calculateNewPartY();
    void generatePartsAhead();
    //void deletePartsBehind();
    
};
}

#endif

/* Guards

Includes

class HillClimbRoad
   public
     MAX_PART_COUNT
     X_ROAD_START
     Y_ROAD_START
     DEFAULT_ROAD_LENGTH

     HillClimbRoad(winWidth, winHeight)
     move(x)
     getPartCoords()
     getPartCount()
     reset()
   private
       partCoords
       addPart(x, y)
       calculateNewPartX(prevPartX)
       double calculateNewPartY()
       void generatePartsAhead()
       deletePartsBehind()

Guards end */